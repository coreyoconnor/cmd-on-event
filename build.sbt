lazy val stage = taskKey[File]("stage")

scalaVersion := "3.4.1"

enablePlugins(ScalaNativePlugin)

import scala.scalanative.build._

nativeConfig := {
  val out = nativeConfig.value
    .withLTO(LTO.thin)
    .withMode(Mode.releaseFast)

  out
}

stage := {
  val exeFile = (Compile / nativeLink).value
  val targetFile = target.value / "cmd-on-event"

  sbt.IO.copyFile(exeFile, targetFile)

  targetFile
}
