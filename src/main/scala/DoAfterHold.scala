import Evaluator.*

object DoAfterHold {

  sealed trait State

  case object Unknown extends State

  case class Release0(tms: Long) extends State

  case class Press0(tms: Long) extends State

  case class Release1(tms: Long) extends State

}

class DoAfterHold(requiredKeys: Set[Key],
                  durationMs: Long,
                  actions: Seq[Action]) extends Evaluator {

  import DoAfterHold._

  private var state: State = Unknown

  def keysDown(keys: Set[Key], ts: Long, tus: Long): Seq[Action] = {
    val tms = toMs(ts, tus)

    if ((requiredKeys & keys).isEmpty) {
      Seq.empty
    } else {
      val (nextState, doActions) = state match {
        case Unknown => (Press0(tms), false)

        case Release0(priorTMs) => (Press0(tms), false)

        case Press0(priorTMs) => (Press0(tms), false)

        case Release1(priorTMs) => (Press0(tms), false)

      }

      state = nextState

      if (doActions) actions else Seq.empty
    }
  }

  def keysUp(keys: Set[Key], ts: Long, tus: Long): Seq[Action] = {
    val tms = toMs(ts, tus)

    if ((requiredKeys & keys).isEmpty) {
      Seq.empty
    } else {
      val (nextState, doActions) = state match {
        case Unknown => (Release0(tms), false)

        case Release0(priorTMs) => (Release0(tms), false)

        case Press0(priorTMs) => {
          val d = tms - priorTMs

          if (d > durationMs) {
            (Release1(tms), true)
          } else {
            (Release0(tms), false)
          }
        }

        case Release1(priorTMs) => (Release0(tms), false)

      }

      state = nextState

      if (doActions) actions else Seq.empty
    }
  }
}
