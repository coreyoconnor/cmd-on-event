{ pkgs ? import <nixpkgs> {}
, src ? builtins.filterSource ( path: type:
  type != "directory" || (baseNameOf path != ".git" &&
                          baseNameOf path != "target" &&
                          baseNameOf path != ".sbt" &&
                          baseNameOf path != ".ivy" &&
                          baseNameOf path != ".metals" &&
                          baseNameOf path != ".bloop" &&
                          baseNameOf path != "alldocs")
  ) ./.,
  # sbt builds, as is, are unstable (? really?)
  # so this needs to be updated more frequently than I'd expect.
  outputHash ? "0w00mwfihqg75d2javx4izw11drgd8xvq6x05chxx98k3855r1qi"
}:
let
  stdenv = pkgs.stdenv;
in rec {
  cmd-on-event = stdenv.mkDerivation rec {
    name = "cmd-on-event";
    inherit src;

    LLVM_BIN = pkgs.clang + "/bin";
    SOURCE_DATE_EPOCH = 315532800;

    buildInputs = with pkgs; [
      stdenv
      sbt
      openjdk
      boehmgc
      libevdev
      libunwind
      clang
      ccls
      which
      zlib
    ];

    buildPhase = ''
      sbt stage
    '';

    installPhase = ''
      mkdir -p $out/bin
      cp target/cmd-on-event $out/bin/
    '';

    outputHashAlgo = "sha256";
    outputHashMode = "recursive";

    inherit outputHash;
  };
}
