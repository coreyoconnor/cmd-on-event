import Evaluator.*

object EchoToStdOut extends Evaluator {
  def keysDown(keys: Set[Key], ts: Long, tus: Long): Seq[Action] = Seq {
    Action(
      name = "keysDown",
      weight = 0,
      f = { () =>
        println(s"keysDown($keys, $ts, $tus)")
      }
    )
  }

  def keysUp(keys: Set[Key], ts: Long, tus: Long): Seq[Action] = Seq {
    Action(
      name = "keysUp",
      weight = 0,
      f = { () =>
        println(s"keysUp($keys, $ts, $tus)")
      }
    )
  }
}
