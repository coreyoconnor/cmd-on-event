import scala.scalanative.posix.poll
import scala.scalanative.posix.sys.time.timeval

import scala.scalanative.unsafe.*
import scala.scalanative.unsigned.UInt

import scala.util.Using.Releasable
import java.nio.charset.Charset

type LibEvDevRef = CSize
type InputEvent = CStruct4[timeval, CShort, CShort, CInt]

object LibEvDevDefines {
	val LIBEVDEV_READ_FLAG_SYNC = 1
	val LIBEVDEV_READ_FLAG_NORMAL = 2
	val LIBEVDEV_READ_FLAG_FORCE_SYNC = 4
	val LIBEVDEV_READ_FLAG_BLOCKING = 8
	val LIBEVDEV_READ_STATUS_SUCCESS = 0
	val LIBEVDEV_READ_STATUS_SYNC = 1
}

@link("evdev")
@extern
object LibEvDev {
  def libevdev_new_from_fd(fd: CInt, refPtr: Ptr[LibEvDevRef]): CInt = extern
  def libevdev_free(ref: LibEvDevRef): Unit = extern
  def libevdev_get_driver_version(ref: LibEvDevRef): CInt = extern
  def libevdev_get_id_vendor(ref: LibEvDevRef): CInt = extern
  def libevdev_get_id_product(ref: LibEvDevRef): CInt = extern
  def libevdev_get_name(ref: LibEvDevRef): CString = extern
  def libevdev_get_phys(ref: LibEvDevRef): CString = extern
  def libevdev_get_unique(ref: LibEvDevRef): CString = extern
  def libevdev_next_event(ref: LibEvDevRef, flags: CInt, inputEventPtr: Ptr[InputEvent]): CInt = extern
}
