type Key = String
type KeyCode = Int

object Main {

  val debug = true
  val trace = false

  def evaluate(event: Event, evals: Seq[Evaluator]): Seq[Evaluator.Action] =
    event match {
      case Event(ts, tus, 1 /* EV_KEY */, c, isDownV) => {
        KeyCodes.codeToKeys.get(c) match {
          case None => {
            System.err.println(s"unknown $c")
            Seq.empty
          }

          case Some(keys) => {
            val isDown = (isDownV > 0)

            evals flatMap { eval =>
              if (isDown) {
                eval.keysDown(keys, ts, tus)
              } else {
                eval.keysUp(keys, ts, tus)
              }
            }
          }
        }
      }
      case e => {
        if (trace) System.err.println(s"ignoring $e")
        Seq.empty
      }
    }

  def loop(devicePath: String, evals: Seq[Evaluator]): Unit =
    EventListener(devicePath) { eventListener =>
      System.err.println(eventListener)

      while (evals.nonEmpty) {
        val event = EventListener.next(eventListener)
        val actions = evaluate(event, evals).sortBy(_.weight).reverse
        val (alwaysActions, possibleActions) = actions.partition(_.weight == 0)

        val selectedActions = possibleActions.toList match {
          case Nil => alwaysActions
          case h :: r => h :: r.takeWhile(_.weight == h.weight) ++ alwaysActions
        }
        selectedActions foreach { action =>
          action.f()
        }
      }
    }

  def parse(input: List[String]): List[Evaluator] = {
    input match {
      case Nil => List.empty
      case ";" :: rest => parse(rest)

      case "echo" :: Nil => EchoToStdOut :: Nil
      case "echo" :: ";" :: rest => EchoToStdOut :: parse(rest)

      case "key" :: keyInput :: "after" :: durInput :: "exec" :: rest => {
        val (cmd, restInput) = rest.span(_ != ";")

        val keysAssumingName = {
          val alts = List(
            keyInput.toUpperCase,
            "KEY_" + keyInput.toUpperCase,
            "BTN_" + keyInput.toUpperCase
          )

          alts filter { alt =>
            KeyCodes.keyToCode.contains(alt)
          }
        }

        if (keysAssumingName.isEmpty) {
          util.Try(KeyCodes.codeToKeys.get(keyInput.toInt)).toOption.flatten match {
            case Some(keys) => {
              val execAction = Evaluator.Action(
                name = "exec",
                weight = durInput.toInt,
                f = { () =>
                  println(s"exec $cmd")
                  val process = java.lang.ProcessBuilder(cmd*).inheritIO.start()
                  println(s"result: " + process.waitFor().toString)
                }
              )

              val eval = new DoAfterHold(
                requiredKeys = keys,
                durationMs = durInput.toInt,
                actions = Seq(execAction)
              )

              eval :: parse(restInput)
            }
            case None => {
              sys.error(s"unknown key $keyInput")
            }
          }
        } else {
          val execAction = Evaluator.Action(
            name = "exec",
            weight = durInput.toInt,
            f = { () =>
              println(s"exec $cmd")
              val process = java.lang.ProcessBuilder(cmd*).inheritIO.start()
              println(s"result: " + process.waitFor().toString)
            }
          )

          val eval = new DoAfterHold(
            requiredKeys = keysAssumingName.toSet,
            durationMs = durInput.toInt,
            actions = Seq(execAction)
          )

          eval :: parse(restInput)
        }
      }
      case _ => sys.error(s"Unknown arguments: $input")
    }
  }

  def main(args: Array[String]): Unit = {
    args.toList match {
      case "from" :: devicePath :: requests => {
        val evaluators = parse(requests)

        loop(devicePath, evaluators)
      }
      case "names" :: Nil => {
        println(f"${"name"}%30s | ${"key code"}%10s")
        println("-" * (30 + 10 + 3))

        KeyCodes.keyToCode.toList.sortBy(_._1) foreach { (key, code) =>
          println(f"$key%30s | $code%10s")
        }
      }
      case _ => {
        System.err.println("""
Invalid or missing arguments.

EG:

> cmd-on-event from /dev/input/event123

Prints a description of event123 to STDERR.

> cmd-on-event from /dev/input/event123 echo

Prints a description of event123 to STDERR.
Then echos all key events to STDOUT.

> cmd-on-event from /dev/input/event123 key BTN_MODE after 5000 exec systemctl restart display-manager \; echo

Executes `systemctl restart display-manager` if BTN_MODE was pressed for more than 5 seconds then
released.

Always echos events to STDOUT.

> cmd-on-event from /dev/input/event123 \
>     key BTN_MODE after 10000 exec reboot \; \
>     key BTN_MODE after 5000 exec systemctl restart display-manager \; \
>     echo

Executes `systemctl restart display-manager` if BTN_MODE was pressed for more than 5 seconds then
released prior to 10 seconds.

Executes `reboot` if BTN_MODE was pressed for more than 10 seconds then released.

Always echos events to STDOUT.

Finally:

> cmd-on-event names

To see a list of all names and keycodes for buttons and keys accepted.

""")
        System.exit(1)
      }
    }
  }
}
