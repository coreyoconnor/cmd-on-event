{
  description = "cmd-on-event";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs";

  inputs.sbt.url = "github:zaninime/sbt-derivation/master";
  inputs.sbt.inputs.nixpkgs.follows = "nixpkgs";

  inputs.systems.url = "github:nix-systems/default";

  outputs = {
    self,
    nixpkgs,
    sbt,
    systems
  }:
    let
      eachSystem = nixpkgs.lib.genAttrs (import systems);
    in
    {
      packages = eachSystem (system:
        let pkgs = nixpkgs.legacyPackages.${system}; in
        {
          default = (sbt.mkSbtDerivation.${system}).withOverrides({ stdenv = pkgs.llvmPackages_18.stdenv; }) {
            pname = "cmd-on-event";
            version = "0.1.0";
            src = self;
            depsSha256 = "sha256-Zta7K+5iUkHebhDv1ldihAcYXxTt7/z96HUfkKrEv2c=";
            buildPhase = ''
              sbt 'show stage'
            '';

            installPhase = ''
              mkdir -p $out/bin
              cp target/cmd-on-event $out/bin/
            '';

            buildInputs = with pkgs; [
              libevdev
              libunwind
            ];

            nativeBuildInputs = with pkgs; [
              which
            ];

            hardeningDisable = [ "fortify" ];
          };
        }
      );
    };
}
