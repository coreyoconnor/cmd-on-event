
## About

EG:

> cmd-on-event from /dev/input/event123

Prints a description of event123 to STDERR.

> cmd-on-event from /dev/input/event123 echo

Prints a description of event123 to STDERR.
Then echos all key events to STDOUT.

> cmd-on-event from /dev/input/event123 key BTN_MODE after 5000 exec systemctl restart display-manager \; echo

Executes `systemctl restart display-manager` if BTN_MODE was pressed for more than 5 seconds then
released.

Always echos events to STDOUT.

> cmd-on-event from /dev/input/event123 \
>     key BTN_MODE after 10000 exec reboot \; \
>     key BTN_MODE after 5000 exec systemctl restart display-manager \; \
>     echo

Executes `systemctl restart display-manager` if BTN_MODE was pressed for more than 5 seconds then
released prior to 10 seconds.

Executes `reboot` if BTN_MODE was pressed for more than 10 seconds then released.

Always echos events to STDOUT.

Finally:

> cmd-on-event names

To see a list of all names and keycodes for buttons and keys accepted.


## dev setup

```
nix-shell
```
