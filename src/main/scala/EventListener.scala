
import scala.scalanative.posix.errno
import scala.scalanative.posix.fcntl
import scala.scalanative.posix.poll
import scala.scalanative.posix.pollEvents
import scala.scalanative.posix.unistd
import scala.scalanative.unsafe.*
import scala.scalanative.unsigned.*

import scala.annotation.tailrec
import scala.util.Using.Releasable

import java.nio.charset.StandardCharsets

import LibEvDev.*
import LibEvDevDefines.*

case class Event(
    sec: Long,
    usec: Long,
    t: Int,
    c: Int,
    v: Int
)

object EventListener {
  private def openPath(path: String): EventListener = {
    val (fd, dev) = Zone {
      val fd = fcntl.open(toCString(path), fcntl.O_RDONLY | fcntl.O_NONBLOCK)

      require(fd > 0, s"failed to open $path")

      val fdRef = stackalloc[CSize](1)

      val rc = libevdev_new_from_fd(fd, fdRef)

      require(rc >= 0, s"failed to open evdev for $path")

      (fd, !fdRef)
    }

    new EventListener(fd, dev)
  }


  given Releasable[EventListener] = new Releasable[EventListener] {
    def release(el: EventListener): Unit = {
      libevdev_free(el.dev)
      unistd.close(el.fd)
    }
  }

  def apply[T](path: String)(f: EventListener => T): T = {
    util.Using.resource(openPath(path)) { eventListener =>
      f(eventListener)
    }
  }

  def next(eventListener: EventListener): Event = {
    @tailrec def go(): Event = {
      val readEvent = Zone {
        val pfd = stackalloc[poll.struct_pollfd](1)
        pfd._1 = eventListener.fd
        pfd._2 = pollEvents.POLLIN
        pfd._3 = pollEvents.POLLIN

        poll.poll(pfd, 1.toCSize, 50)

        val evPtr = stackalloc[InputEvent](1)

        val rc = libevdev_next_event(eventListener.dev,
                                    LIBEVDEV_READ_FLAG_NORMAL | LIBEVDEV_READ_FLAG_BLOCKING,
                                    evPtr)

        if (rc == LIBEVDEV_READ_STATUS_SUCCESS) {
          Some {
            Event(
              sec = evPtr._1._1.toLong,
              usec = evPtr._1._2.toLong,
              t = evPtr._2,
              c = evPtr._3,
              v = evPtr._4
            )
          }
        } else if (rc == LIBEVDEV_READ_STATUS_SYNC || rc == -errno.EAGAIN) {
          None
        } else {
          throw new java.lang.RuntimeException(s"failed to read event: $rc")
        }
      }

      readEvent match {
        case Some(event) => event
        case None => go()
      }
    }

    go()
  }
}

class EventListener(val fd: CInt, val dev: LibEvDevRef) {
  def ok: Boolean = true

  val vendorId = libevdev_get_id_vendor(dev)
  val productId = libevdev_get_id_product(dev)
  val name = Zone {
    fromCString(libevdev_get_name(dev), StandardCharsets.UTF_8)
  }

  override def toString(): String = {
    s"EventListener(fd = ${fd}, name = ${name}, vendor_id = ${vendorId}, product_id = ${productId})"
  }
}
