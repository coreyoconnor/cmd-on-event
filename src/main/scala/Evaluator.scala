def toMs(ts: Long, tus: Long): Long = {
  ts * 1000 + tus / 1000
}

object Evaluator {
  case class Action(
      name: String,
      weight: Int, // biffer is begger except 0 is special
      f: () => Unit
  )
}

import Evaluator.*

trait Evaluator {
  def keysDown(keys: Set[Key], ts: Long, tus: Long): Seq[Action]

  def keysUp(keys: Set[Key], ts: Long, tus: Long): Seq[Action]
}
